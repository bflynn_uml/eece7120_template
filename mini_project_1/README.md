# Mini Project 1:
Due date: 

You have now received your robot! Let’s do something fun with it. Before we do that, though, we have to put it together and calibrate it. This mini-project will serve as a check-off that each of the assembly, initialization, and calibration steps is complete before we move on to later projects. 

## Assemble your robot
[Assembling the Duckiebot](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/assembling_duckiebot_db18.html) 

Make sure that you properly label your robot after picking it up from your instructor, there will be no other obvious way to tell each robot apart.

## Initializing the duckiebot
1. [SD Card Initialization](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/setup_duckiebot.html)


Make sure you choose a unique name for your duckiebot (The name cannot be Ducky as it will cause strange behavior) It should look something like this:

dts init_sd_card --hostname <hostname> --wifi duckietown:quackquack,<my_homenetwork>:<my_homepassword>

This step will take a while. It is safe to leave the computer alone for a bit and come back to it periodically to see if it is asking for any input. Eventually you will have to choose the disk that you wish to put the files on as indicated in the instructions on the Duckiebot Initialization page.

Make sure that your battery has a full charge before doing the next step as it will likely take over an hour for the duckiebot to boot the first time. 

Also note that the battery cannot be charging during this process. It will not provide sufficient current while charging.

2. Insert the initialized SD card into the duckiebot with the power turned off. (Step 3.3)

Without the SD card inserted into the raspberry pi, plug in both of the the power usb cables to the portable battery bank. Some lights will turn on, but it is safe to hold the button on the side of the battery until all of the battery lights and duckiebot LEDs turn off. 

Once all lights are off, insert the SD card into the SD card slot on the raspberry pi and then hit the button on the side of the battery again. The battery and duckiebot will turn on and you will begin to see LEDs flash on the bot as well as on the raspberry pi.

The raspberry pi will have a RED LED next to the blinking GREEN LED on the board as indicated in the instructions. Even though the instructions do not mention this RED LED, this is normal and shows that the board has power. You will see the GREEN LED begin to flash at random intervals.

You can now follow the rest of the instructions as normal.

Make sure that you do not unplug or turn off the duckiebot before the entire process is complete. Assuming you have set up the network properly, you will know that you successfully set up the duckiebot once the GREEN LED is on and the RED LED is iff, as indicated in the instructions, type 'ping <your_hostname>' into your terminal. If you get a response then everything is working properly. 

Follow through with troubleshooting if you do not get this result.

If you can ping your robot, you can now ssh into it by following the instructions in section 3.4. You can now shut the robot down by running the command 'ssh <your_hostname> sudo poweroff'

This is the only safe way to shut down the robot so you should only ever turn it off this way. After inputting the command, give the robot about 30 seconds and wait until the GREEN LED stops flashing.
        
[Networking setup](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/duckiebot_network.html) (NOTE: We have our own router which can be used in the teaching lab. You may also want to set your duckiebot up on your home network in the previous step. This means we are using Option 1 in the linked reference.)
        
3. Set up some useful tools for monitoring the robot and docker image state. If you ever notice something strange about the way your robot is behaving, check these first and you may see that a container or process is stopped or broken. It's the easiest thing to check first.

[Set up Portainer](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/docker_setup.html)
        
[Set up your robot’s dashboard](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/duckiebot_dashboard_setup.html)

## Finally, make your robot move! 
[Making your Duckiebot Move](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/rc_control.html)

NOTE: keyboard control from mission control does not seem to work. There are also issues using Mac. Ubuntu works well, from VM or native

1. Use Option 1 - in section 7.1 from the previous link in order to get the robot moving.

2. Take a video of your robot moving around! Turn it in with this assignment.
3. [Calibrate your camera](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/camera_calib.html)
4. [Calibrate your robot’s kinematics](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/wheel_calibration.html) 

You can test out your calibrations by using the [lane following demo](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/demo_lane_following.html)
    
5. Take a log of your best validation run from the previous step.
  1. On your laptop, run $ dts start_gui_tools
  2. Inside of the gui tools container, run: $ rosbag record -o demo_log /DUCKIEBOT_NAME/camera_node/image/compressed /DUCKIEBOT_NAME/wheels_driver_node/wheels_cmd
  3. Make sure to replace DUCKIEBOT_NAME with the name of your robot
  4. Try to keep the log relatively short as it is recording camera data so it might get large quickly. A single run of lane following is sufficient.

EXPLANATION: This command records rostopics to a file for replay later. To record other topics, run $ rostopic list and put other topics instead of or in addition to the ones listed. For this log, just give me the camera image and wheel commands
        
The -o flag gives the log name a prefix to make it easier to find later. For this log, we will name it "demo_log" but you may wish to use different names when recording logs later. The log will record to a file name like demo_log_DATE_TIME.bag. You can find the exact name by typing $ ls
        
You can replay it in any ROS setup (docker or native Ubuntu with ROS installed) by using the "play" command instead of record. More information here: http://wiki.ros.org/rosbag/Commandline
        
Copy that file out of the docker container into your Linux install. From a terminal in your Linux install (not robot), run: 

'docker ps'
        
and find the name of the interactive_gui_tools container (it should be something like interactive_gui_tools_DUCKIEBOT_NAME)
        
From the same terminal, run:

docker cp CONTAINER_NAME:/home/software/LOG_NAME .
        
NOTE: the last "." is very important in the above command. That tells it to put the file in whatever directory you are in now. You can move it wherever you want later or replace the dot with the path you prefer. Just remember where it is so you can upload it.
        
This assumes that you did not change directories after starting the gui tools in step 2. If you did, modify the directory path accordingly.
        
You will need to know the name of the container from the previous step and the name of the log file from step 2.
        
[Official Duckietown log information](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/take_a_log.html)

This option has not been working for some people so use whichever method works for you.
    
## Write a program to make the duckiebot move
Now, to apply this knowledge! Write a ROS node to make your robot drive in a repeatable pattern (to the extent possible with the calibration parameters). The pattern is up to you. You will demonstrate this pattern in class.

Refer back to homework 1 where you went through the turtlebot simluation example. Much like how you used rostopic list to see which topics were being published and rostopic echo to listen to the messages published on those topics, you can determine which topic drive commands are published on to the duckiebot in order to make it move when doing the lane following demo or joystick controller.

[Understanding ROS nodes](http://wiki.ros.org/ROS/Tutorials/UnderstandingNodes)

This tutorial will introduce you to process of running nodes through a simulation caled turtlesim. 

[Understanding ROS topics](http://wiki.ros.org/ROS/Tutorials/UnderstandingTopics)

The next page will continue to use turtlesim to explore ROS topics, 
a component you will need in order to talk across nodes and machines.

You can then use the same structure that you defined in homework 2 to create a node that publishes messages on that topic to the nodes that control the duckiebot's movements.

The movement doesn't have to be complex, but it should be a movement you can describe and repeat with some loose degree of accuracy based on how well your kinematics calibration came out.

For more insight into how duckietown accomplishes this task, take a look at the Lane Controller Node that you can find inside of the duckietown software folder in the catkin\_ws.

[Lane Controller Node](https://github.com/duckietown/Software/blob/master19/catkin_ws/src/10-lane-control/lane_control/scripts/lane_controller_node.py)

Make sure you tag your code with mp_1

## Completing the assignment
Turn in one PDF with:

A pdf with:

1. The commit that should be graded in your git repo

2. A description of your pattern.

3. A paragraph describing the challenges you encountered with this mini-project and your efforts to overcome them

4. The video from 2f

5. The log (rosbag) from 5

