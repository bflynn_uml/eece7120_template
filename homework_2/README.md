# Homework 2:
Due date: 

## In this assignment we will make a basic ROS package
This will involve setting up your workspace, creating a package, creating a message type, and creating three nodes.

Visit the ROS.org [ROS tutorials](http://wiki.ros.org/ROS/Tutorials)

Another friendly reminder that if you are not familiar with ROS, or want a refresher, this is the best place to start.

## Add your instructor to your git repo
In order to grade your work, your instructor will need access to your private git repo.

Github:
Navigate to 'Settings' and then 'Collaborators' and enter paulrobinette and click 'Add collaborator'

Bitbucket:
Navigate to 'Settings' and then 'User and group access' and add paulrobinette in the users field

## Getting set up
Create your catkin workspace if you haven’t already done this in homework assignment 1

[Set up your ROS environment](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment)

Note that you can do this within a docker container associated with duckietown or a ROS install on your machine/VM. 

please use ROS Melodic on ubuntu 18.04 if you have not already set up your ROS environment.

Now, let’s set up a ROS package.

[Creating a ROS package](http://wiki.ros.org/ROS/Tutorials/CreatingPackage)

[Building a ROS package](http://wiki.ros.org/ROS/Tutorials/BuildingPackages)

## Creating a custom message

Navigate to the homework_2 package and locate the folder called 'msg' where you will find an example custom message file called Example.msg. 

You should create a new custom message file and name it something more specific as you will reference it within your code and you want to know what your message does for you without having to open up the specific file.

Your task is to create a custom ROS message that contains:

  1. A string labeled id

  2. A Point labeled position

You can reference the ROS tutorials for more information on creating a message, using that message type in your code, and modifying your CMakeLists.txt file to allow the use of your new message type. 

You can ignore services for now. They are a useful component if you wish to explore them on your own however we will not need them for this class.

[Creating a ROS message and service](http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv)

You will also need to view the documentation for a Point in order to complete your custom message type. 

You will need this information for when you have to publish or subscribe to a Point message type in the following section: creating your ROS nodes.

[geometry_msgs/Point message](http://docs.ros.org/melodic/api/geometry_msgs/html/msg/Point.html)

## Creating your ROS nodes

Reference the following ROS tutorial page for a guide on how to create a publisher node and a subscriber node in python. You will need one publisher node, one subscriber node, and one node that both publishes and subscribes in order to complete this assignment.

The tutrial page does not show you how to do subscribe and publish within the same node, however you should experiment and look online for possible solutions to this issue as it will be a useful resource moving forward. Keep in mind that ROS is open-source for a reason. Your implementation should be your own but there is no need to reinvent the wheel.

[Writing a simple Publisher and Subscriber](http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29)

You will need to create three ROS nodes:
  - Node 1: publishes points (as geometry\_msg/Point type) with random x,y,z values on topic '/obstacles_detected'
  - Node 2: subscribes to '/obstacles\_detected', ingests those points, adds a unique identifier to them (your choice of format) and publishes this new data structure as YOUR MESSAGE TYPE from question 4 on topic '/registered_obstacles'
  - Node 3: Subscribes to '/registered\_obstacles' and logs an info-level message to rosout for each one in a human readable format such as: 'Detected obstacle <id> at position <x>,<y>,<z>'

An example publisher and subscriber node has been placed within the src directory of the homework\_2 package. This is where you should be placing any nodes you create. Think of this as a starting point for Node 2. Nodes 1 and 3 do not need both a publisher and subscriber.

You should change the name of the publisher and subscriber node or create an entirely new one. Make sure that the name of the node reflects either the name of the python script or is reflective of what the node is expected to do. When the nodes are running, you want them to have descriptive names in case you need to inspect node performance or do any debugging while nodes are running.

## Optional: Create a launch file

While it is not a graded component for this assignment, creating a launch file that starts all of your relevant nodes and a roscore at the same time will be required for future assignments and is general good practice. 

Check out the following tutorial page and scroll down to section 2.2 where you will be introduced to the method of launching a launch file as well as creating one for your own nodes. 

A simple launch file caled pub\_and\_sub.launch has been placed in the launch folder within the homework_2 package. This launch file will bring up one node, try to add in the other two nodes once you've created them. It will be required later and will save you time when running your nodes and collecting data.

[Using rqt_console and roslaunch](http://wiki.ros.org/ROS/Tutorials/UsingRqtconsoleRoslaunch)

Without the launch file you would need 4 terminals open:

  1. A roscore
  2. Node 1
  3. Node 2
  4. Node 3

However any time you use the roslaunch \[package] \[launch file] command, you automatically start up a roscore and do not need individual terminals open for each of the nodes contained within the launch file.

In future assignments you will learn how to launch other launch files along with nodes.

## Pushing your code up to your Git repository
The first thing you need to do is initialize your workspace with Git

Navigate to your workspace's src directory in a terminal and enter:

'git init'

You will now notice a .git folder in your src directory.

You can either add specific files or add every file in this directory and its sub-diretories to your 'order'

git add . (the period is necessary) will add all files in the current and sub-directories

git tag <some_useful_tag> will add a tag to the current submission

git commit -m "some useful update information" will add a message to each updated file to help track changes

git remote add origin <url_of_your_repo> will add the location that you are pushing/pulling your code to and save it so that you only need to do this once

git push -u origin <your_branch> (usually this will be master unless you make additional branches and want to separate your code by feature for example)

## Tagging your submissions
In addition to pushing code up to different branches, you are also able to utilize tags in order to keep track of uploads and code segments.

As shown above, it is relatively easy to add a tag to your current files that are ready for upload

git tag (alone) will show you all of the tags associated with the current package

git tag <some_tag> will create the new tag

In order to push up your code with a tag (something you will have to do for all future submissions), you must include it in your git push command:

git push -u origin <your_branch> <some_tag>

If my branch is 'master' and my tag is 'hw2', then my push command will look like this:

git push -u origin master hw2

## Completing the assignment
Turn in to Blackboard a PDF with:

  1. Your name

  2. Your repo name (should be the same as you submitted in HW1)

  3. Your package name (should be homework_2)

  4. The tag for this homework (should be hw2)

