# Homework 8:
Due date: 

## In this assignment we will continue tuning a lane filter
The source of the code for this assignment is the same as from homework 7, however this time you will be making adjustments to the hough transform input values in order to improve line edge detection.

## Run the python script without changing any values for a baseline
You can run the code by changing directories to the homework_8 src folder and typing:

./color_and_line.py <filename>

This should work in whatever system you are currently using for ROS code. The output will display on the screen and write to files in the same location as the provided images and python script. 

This would be a good time to either copy over your finished code from homework 7 or update the code in your homework_8 package to reflect the changes you made in homework 7. This time you will only be updating the hough transform parameters

Note again that each time it is run with a new image, it will overwrite the previous output files.

## Tune the hough transform parameters in the get_lines function
Tune the Hough transform parameters in the get_lines() function at the top of the script until you have clear lines parallel to the lane for white and yellow lane markers. Do not worry about additional lines, as long as the inner lane markings are well represented.

Remember again that you can check the documentation for any of these functions in order to better understand the values that you are changing and their impact on the resulting images. 

[Feature Detection Documentation](https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html)

You will need to scroll down a bit to find the HoughLines() function.

There are a few other optional parameters to tune. Feel free to change the code in any way that improves the output.

It may also be useful to limit the number of images that pop up each time you run the script. Note that and line running the command vc2.imshow() is only showing you the image that was last processed. Since you are only editing the hough transform parameters for the main body of the assignment, you can optionally suppress or comment out most of the imshow() commands asie from those showing edges or white/yello output images.

Experiment with these lines in order to reduce clutter if you desire.

## Completing the assignment
Turn in one PDF with:

1. The final white and yellow output images (white_output.png, yellow_output.png) for EACH input image. (15 points for each image pair)
2. A brief description of the parameters that worked and how you got them (15 points)
3. Also upload your final code to your repository and tag it "hw8" (10 points)



