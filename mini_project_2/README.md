# Mini Project 2:
Due date: 

## Create a new lane position PID controler 
We have seen the robots move along the Duckietown road and have noticed that the existing lane position controller is insufficient. Write a new lane position control that uses PID to maintain its position in the center of the lane. 

This will replace the existing [lane_controller_node](https://github.com/duckietown/Software/tree/master19/catkin_ws/src/10-lane-control/lane_control/) with your own node. 

Create a docker image for this by performing the following steps:

1. Clone the duckietown software repo onto your computer somewhere if you haven't already:

git clone https://github.com/duckietown/Software.git

2. And then link the catkin\_ws directory in the Software repo (the workspace that the duckiebot and duckietown shell scripts use) to your own catkin repository:

ln -s ~/<path-to-duckietown-software>/catkin_ws/src/ ~/catkin_ws/src/duckietown

You will now see a file called duckietown which will link to the actual location of the Software folder. This will allow you to include packages from the duckiebot Software repository in your own custom packages without placing those packages inside of your atual workspace.

This will also allow you to launch those pre-existing launch files and nodes that are needed within the duckietown architecture in order to get your new lane\_controller\_node working as a replacement for the existing one without needing to recreate the entire environment in your new custom workspace.

3. Now create your own package that contains a startup script that launches the lane following nodes that you want, but your node in place of lane\_control\_node. 

Inside of the base src directory in the template repository you will find a Dockerfile. The Dockerfile specifies which packages will be built into a docker image that you will be sending over to the duckiebot in order to run your code.

  - HINT: Look into the duckietown\_demos package inside of the 70-convenience\packages folder within the Software folder for the launch file that you will replicate in order to launch yournew lane position controller node

4. If you are not using the provided mini\_project\_2 package, examine the dockerfile and make the necessary adjustments. You need to point to the location of the package you wish to put onto a docker image and load ontoyour duckiebot.

5. Build your new docker image by changing directories to the directory in which your Docker file resides and enter the following command:

  - docker build -t <image_name>:<image_tag> .

You can choose any image name and tag that you will be able to remember that represents the code you are sending to your robot.

If all of your files are correct, this process will only take a few miuntes at most and you will see progress in your terminal.

6. Copy your docker image to your robot:

  - docker save <image_name>:<tag> | ssh -C <duckiebot> docker load

You will not be getting any updates in your terminal. The process may take upwards of 30 minutes and you unfortunately will not know when the process is complete until you are able to input commands again in your terminal.

If the process takes more than an hour, hit Ctrl + C and respond to any errors that are output into the terminal.

7. You can now run your docker image by using the following command:

  - docker -H <duckiebot>.local run -it -v /data:/data --rm --net=host <image_name>:<image_tag> bash

You can open up multiple instances from different terminals if you desire, but now you are able to launch any launch files or run any nodes after sourcing your custom workspace (custom\_ws if you did not alter the Docker file).

This is not the only way to run code on the robot but you are able to launch this docker image whenever you wish as it gets saved onto the robot. Keep in mind that any changes made on the robot once the docker image has been loaded will not be saved, you will need to upload the docker image again with a new tag/image name if you make any substantial changes so try to do as much on your local machine first as possible to save time.

Demonstrate your solution in class. 

Ensure your code is in your git repo and submit a report that describes your package, how well it works, and the problems you encountered along the way to Blackboard. Create a tag in your git repo for this submission (can be the same as Part 1 but does not have to be). Make sure you include your tag label in the report.

## Completing the assignment
250 points possible

Git tag: 10 points

Docker image: 40 points

ROS Package: 100 points

  - Algorithm: 50 points

  - ROS Node: 30 points

  - Launch file: 20 points

Demonstration: 25 points

Accuracy of result: 25 points

Report: 50 points



