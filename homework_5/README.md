# Homework 5:
Due date: 

## In this assignment we will tune a PID controller 
There is a new Python program that simulates a dynamic system (modeled on a simple car) that moves in one dimension in the class git repo. Included is a PID controller for position. 

Using this simulator, experiment with tuning the PID loop. Specific instructions for running are below. 

## Get the simulator onto your local machine
To get the Python simulator, run git pull from within the [class repository](https://bitbucket.org/paulrobinetteUML/eece7120-fall2019/src/master/) on your computer

If you have not yet cloned the class repository:

change directories into the src folder in your catkin_ws or otherwise at the base of your file structure

use the git clone command:

git clone https://bitbucket.org/paulrobinetteUML/eece7120-fall2019.git

## Running the simulator
You can run this simulator using the following command:
python3 vehicle_motion.py kp=1.0 ki=0.0 kd=0.0 time=100 p0=0 v0=0 desired=30 noise=0.0

Make sure you change directories to the location of the python script first

## Examine the code and make adjustments
Note that not all parameters are needed. Below is a description of the parameters. The default values for each is listed in the description:

Usage: vehicle_motion.py

Options:

   kp=1.0 - set kp

   kd=0.0 - set kd

   ki=0.0 - set ki

   time=100.0 - set time for simulation to run

   desired=30 - set desired position

   p0=0.0 - set initial position

   v0=0.0 - set initial velocity

   noise=0.0 - set the noise multiplier

## Completing the assignment

Please turn in a PDF to Blackboard with the answers to all questions in this assignment. Include output graphs to support your responses.

1. (25 points) Tune the PID loop to stabilize the system. Try to get the overshoot, steady state error, rise time, and settling time as small as possible. State the kp, ki, kd parameters you found and how you got them. Also include a graph of the output.

2. (25 points) Can you set the control (kp, ki, kd) parameters such that the system is unstable? Try to find the smallest changes you can make to the parameters to make it unstable (don't just set everything to 1,000,000!) State the parameters that made it unstable and your intuition for why this is. Also include a graph of the output.

3. (25 points) Now add noise. Set the noise multiplier parameter to 10 (noise=10). This adds random values between -10 and 10 to the system. Can your controller handle this? Submit a graph of the output. Tune it to handle it if not. Report the new parameters and submit the graph after retuning.

4. (25 points) Now let's find out how much noise the system as a whole can handle. Increase the noise multiplier until the system is unstable. Try to retune it, then increase the noise more. Submit graphs each time you retune the PID loop. What do you think is the maximum noise that this system can handle and still be controllable?

5. (optional) Feel free to adjust any other parameters in the Python script to explore further!



