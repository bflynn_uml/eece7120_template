# Homework 6:
Due date: 

## In this assignment we will calculate and plot odometry from a simulated vehicle 
The package homework\_6 contains two nodes: one that generates odometry messages based on the contents of a CSV file (wheel\_tick\_pub.py) and one that contains the skeleton of code necessary to generate odometry from these messages (odom\_calc.py).

You will be trying to match results to the image labeled HW6\_output.png which is also located within your homework\_6 package.

## Completing the assignment
1. Add the code necessary to calculate odometry. Use a baseline of 0.1m (so 2L=0.1, L=0.05 in the equations in the slides) for the wheel distance.

2. Test this code and ensure that your final graph looks similar to HW6\_output.png. Save a copy of the graph that you generate.

  - (optional) Experiment with different values in the csv file

3. Push the code to your git repo. Add a tag for hw6. Submit to blackboard a PDF report that explains your code, includes the tag name, and includes your graph.

Rubric:

Git tag: 10 points

Working ROS package with launch file (can use the one provided): 10 points

Odometry code: 50 points

Report with graph: 30 points

