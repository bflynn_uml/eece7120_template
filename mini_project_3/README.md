# Mini Project 3:
Due date: 

## Compare the results of your code from homework 7 and 8 to the built-in line detector node
in HW7 and HW8, you worked on tuning a different lane filter than the built in method. The algorithm is largely the same, but your numbers are likely different. Let's see how it compares to the Duckietown lane filter.

If you haven't already, run the indefinite navigation demo in the Duckietown set up in DAN 407. While this can be run from dts, it is best run from a docker container using roslaunch.

It is best to remove your MP2 code from your docker container first (this is as easy as sourcing catkin_ws instead of custom_ws at the start of your docker session).

Then run roslaunch duckietown_demos indefinite_navigation.launch
        
You will need to start dts duckiebot keyboard_control DUCKIEBOT_NAME in a separate terminal and press 'a' to start autonomy.
    
## Create a ROS package that:
1. Contains a node that:
  - subscribes to "image_transformer_node/corrected_image/compressed"
  - Publishes the yellow and a white hough transform images as completed in HW8 in their own topics (you define)
2. Contains a launch file that:
  - Imports the indefinite navigation launch file
  - Sets the parameter "/hostname/line_detector_node/verbose" to true AFTER the indefinite navigation include. This outputs debug images we will use in the next step. (see this for reference http://wiki.ros.org/roslaunch/XML/param
    - Performs any remaps necessary to make the subscription above possible
  - Starts your node

## Publishing and subscribing to the correct message types
For your ros node you will need to subscribe to a topic which carries CompressedImage messages.

you cannot use cv2 functions on CompressedImages by default so you will need to convert the CompressedImage into a data type that works well with cv_image functions.

Take a look at the following cv_bridge documentation:

[cv_bridge Documentation](http://docs.ros.org/melodic/api/cv_bridge/html/python/)

You can see that there are built in functions for these types of operations. Try out some of the conversion functions and see which give you a better result. You will be transmitting images over a wireless network so you can expect some latency issues but if you are careful, you can forcibly limit the rate at which images are saved and processed, improving your results.

## Create a new docker image as in mini project 2 with your new code
Ensure that you can launch the indefinite navigation demo alongside your node that publishes the hough transform images you generate with your code from homeworks 7 and 8. You should be able to launch everything from one terminal and then use a second terminal to run dts start\_gui\_tools and spin up three rqt\_image\_viewer nodes to do your visual comparisons with

## Compare your node with the Duckietown node. Note that the Duckietown node outputs in a different visual format than our node, but you should be able to compare them visually.
        
1. While your launch file above is running, start up dts start\_gui\_tools DUCKIEBOT\_NAME in another terminal
        
2. Start three instances of the image viewer using "rqt_image_view &" The '&' makes it run in the background, allowing you to enter more commands.
        
3. Select the Duckietown debug image topic in one viewer ("/hostname/line_detector_node/image_with_lines") and your topics in the other two.
        
4. Place your duckiebot in several locations inside Duckietown and see how the images compare
        
5. Run the autonomy mode (press 'a' in keyboard control) and watch the difference.
        
Demonstrate this live in class or by appointment.

## Completing the assignment
Your ROS package in your git repo:
1. Package setup: 40 points
2. Launch file: 50 points
3. Source code: 100 points
4. git tag: 10 points
5. A PDF with several images taken from step 3.4 above and a discussion about how you could improve your results: 150 points
5. Demonstration (video may be acceptable in extenuating circumstances) 50 points

