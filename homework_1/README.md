# Homework 1:
Due date: 

## Create a git repo for your code. 
Give your repository a unique name that you don’t mind sharing.

Use your official UML email for the account. Make sure the repo is PRIVATE until the semester is over. 

As of 2019, Github allows each user to have 3 free private repositories. In either case, you will need to add the professor as a contributor for the ability to view and clone your code and grade your assignments.

## Create appropriate logins for these services using your official UML email:
[Duckietown](https://www.duckietown.org/site/register)
[Docker Hub](https://hub.docker.com/)

## Install docker and docker-compose
[Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
[Docker Compose](https://docs.docker.com/compose/install/)

## Install dts and otherwise complete duckietown laptop install 
[Duckietown Laptop Setup](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/laptop_setup.html)

## Install ROS in your linux environment or in a container or VM of your choice:
[Install ROS Melodic on Ubuntu 18.04](http://wiki.ros.org/melodic/Installation/Ubuntu) 

Perform the Desktop-Full install

Visit the ROS.org [ROS tutorials](http://wiki.ros.org/ROS/Tutorials)

If you are not familiar with ROS at all, this is the best place to start. 

[Set up your ROS environment](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment)

  The final step on this page has you create a ROS workspace which is where you will store your code in packages. 

  It is also where you will run your code from. Feel free to name your workspace catkin\_ws as is done in the tutorial. 

  If you choose to give your workspace a different name, just remember to swap your custom name in any place the tutorial has you enter catkin\_ws.

  The next page will show some tips on navigating the ros filesystem. This will help you out but is not required reading. It is fine to revisit this later if it seems confusing.

## Creating a ROS package

These two pages will guide you through creating and building a ROS package.
     
[Creating a ROS package](http://wiki.ros.org/ROS/Tutorials/CreatingPackage)

[Building a ROS package](http://wiki.ros.org/ROS/Tutorials/BuildingPackages)

## Understanding ROS infrastructure through the ROS tutorials:
[Understanding ROS nodes](http://wiki.ros.org/ROS/Tutorials/UnderstandingNodes)

This tutorial will introduce you to process of running nodes through a simulation caled turtlesim. 

[Understanding ROS topics](http://wiki.ros.org/ROS/Tutorials/UnderstandingTopics)

The next page will continue to use turtlesim to explore ROS topics, 
a component you will need in order to talk across nodes and machines.

## Completing the assignment
Turn in to Blackboard a PDF with:

  1. Your name

  2. Link to your git repo

  3. Your username on Bitbucket or Github

  4. Your Duckietown username

  5. your Docker Hub username

  6. A screenshot of the output of 'rostopic echo' from the ROS tutorials: Understanding ROS Topics: section 2.4

## Preparing for next class
Review the [Duckiebot Operation Manual](https://docs.duckietown.org/DT19/opmanual_duckiebot/out/assembling_duckiebot_db18.html)

Be prepared to start building your Duckiebot next class in the lab section.
    
