# Homework 7:
Due date: 

## In this assignment we will begin to tune a lane filter
There is a file located in your homework_7 package, but it is poorly tuned. Included with the pyton script are five images of lanes in Duckietown.

## Run the python script without changing any values for a baseline
You can run the code by changing directories to the homework_7 src folder and typing:

./color_and_line.py <filename>

This should work in whatever system you are currently using for ROS code. The output will display on the screen and write to files in the same location as the provided images and python script. 

Note that each time it is run with a new image, it will overwrite the previous output files.

## Tune the color filtering values
1. Tune the white and yellow filter parameters so that the lanes are better represented.

These are the lines that read: 

white_filter = cv2.inRange(hsv, (0,50,0), (255,180,255))

and

yellow_filter = cv2.inRange(hsv, (0,50,0), (140,255,255))

You can use the following page to experiment with different HSV values and get a feel for the impact that changing each value has on your end result.

[HSV color picker](https://alloyui.com/examples/color-picker/hsv.html)

2. Tune the thresholds for Canny edge detection until you are satisfied with the output.

This line reads:

edges = cv2.Canny(image, 0, 300, apertureSize=3)

For more information on what each of the inputs for the canny function are, refer to the cv2.canny() function documentation

[Feature Detection Documentation](https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html?highlight=canny)

Canny feature detection is right at the top.

There are a few other optional parameters to tune. Feel free to change the code in any way that improves the output.

## Completing the assignment
Turn in one PDF with:

1. The final white and yellow output images (white_edges.png, yellow_edges.png) for EACH input image. (15 points for each image pair)

2. A brief description of the parameters that worked and how you got them (15 points)

Also upload your final code to your repository and tag it "hw7" (10 points)
